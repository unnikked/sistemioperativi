package esercitazioni.esercitazione04.lettoriscrittori;

/**
	Implementazione di MemoriaCondivisa tramite Semafori
*/

import java.util.concurrent.Semaphore;

public class MemoriaCondivisaSem extends MemoriaCondivisa {
	private int numLettori = 0;
	private Semaphore mutex = new Semaphore(1);
	private Semaphore scrittura = new Semaphore(1);
	public void inizioScrittura() throws InterruptedException {
		scrittura.acquire();
		System.out.println("Thread " + Thread.currentThread().getId() + " inizia a scrivere");
		System.out.println("\tNumero lettori: " + numLettori);
	}

	public void fineScrittura() throws InterruptedException {
		System.out.println("\tNumero lettori: " + numLettori);
		System.out.println("Thread " + Thread.currentThread().getId() + " finisce di scrivere");
		scrittura.release();
	}

	public void inizioLettura() throws InterruptedException {
		mutex.acquire();
		if (numLettori == 0) scrittura.acquire(); //blocca scrittori 
		numLettori++;
		System.out.println("Thread " + Thread.currentThread().getId() + " inizia a leggere");
		mutex.release();
	}

	public void fineLettura() throws InterruptedException {
		mutex.acquire();
		numLettori--;
		System.out.println("Thread " + Thread.currentThread().getId() + " finisce di leggere");
		if (numLettori == 0) scrittura.release();
		mutex.release();
	}
	
	public static void main(String[] args) {
		new MemoriaCondivisaSem().test(20, 8);
	}

}
