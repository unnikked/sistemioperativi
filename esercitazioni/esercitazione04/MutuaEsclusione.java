package esercitazioni.esercitazione04;

/**
	Implementare in Java gli scenari descritti nei
	due esempi di mutua esclusione e
	sincronizzazione descritti in questa
	esercitazione, in cui due le istruzioni A e B,
	eseguite dai thread p1 e p2, consistono in una
	stampa su terminale di “A” e “B”
	rispettivamente.
*/

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

class Filo extends Thread {
	private final int ATTESA_MIN = 1, ATTESA_MAX = 3;
	private Semaphore sem;
	private String name;
	private Random random = new Random();
	
	public Filo(Semaphore sem, String name) {
		this.sem = sem;
		this.name = name;
	}
	
	public void run() {
		while(true) {
			try {
				attendi(ATTESA_MIN, ATTESA_MAX);
				sem.acquire();
				System.out.println("Thread id: " + getId() + " stampa " + name);
				sem.release();
				attendi(ATTESA_MIN, ATTESA_MAX);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	private void attendi(int min, int max)
			throws InterruptedException {
		TimeUnit.SECONDS.sleep(random.nextInt(max - min + 1) + min);
	}
	
}

public class MutuaEsclusione {
	public static void main(String[] args) {
		Semaphore s = new Semaphore(1, true);
		Filo A = new Filo(s, "A");
		Filo B = new Filo(s, "B");
		
		A.start();
		B.start();
	}
}

/**
	Con Semaphore s = new Semaphore(1); garantiamo sì la mutua esclusione
	ma non riusciamo a garantire che le istruzioni A e B vengono stampate
	alternativamente poiché permettiamo il barging, l'output sarà simile a
	questo:
		Thread id: 8 stampa A
		Thread id: 9 stampa B
		Thread id: 9 stampa B
		Thread id: 8 stampa A
		Thread id: 8 stampa A
		Thread id: 9 stampa B
		Thread id: 9 stampa B
		Thread id: 8 stampa A
		
	Con Semaphore s = new Semaphore(1, true); importiamo la fairness a true
	cioè garantiamo il risveglio dei Thread sospesi in ordine FIFO, questa
	soluzione è più onerosa in termini di risorse. L'output sarà del tipo:
		Thread id: 9 stampa B
		Thread id: 8 stampa A
		Thread id: 9 stampa B
		Thread id: 8 stampa A
		Thread id: 9 stampa B
		Thread id: 8 stampa A
		Thread id: 9 stampa B
		Thread id: 8 stampa A	
*/
