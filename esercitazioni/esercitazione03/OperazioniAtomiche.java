package esercitazioni.esercitazione03;

/**
	Si realizzi un’applicazione multithreaded che, data una
	matrice rettangolare nxm di interi inizializzati a zero, inizializzi
	ed avvii:
		● n thread, ognuno dei quali riceve una determinata riga della
		matrice e decrementa di uno ogni elemento della riga;
		● m thread, ognuno dei quali riceve una determinata colonna
		della matrice ed incrementa di uno ogni elemento della
		colonna.
	Si implementi prima una soluzione non thread-safe, usando
	una matrice di int, e poi una soluzione thread-safe, usando
	una matrice di AtomicInteger. Si definisca quindi un main che
	stampi su terminale la matrice al termine dell'elaborazione, in
	ciascuno delle due soluzioni.
*/
class DecrementaRiga extends Thread {
	private int[][] m;
	private int rig;
	
	public DecrementaRiga(int[][] m, int rig) {
		this.m = m;
		this.rig = rig;
	}
	
	public void run() {
		for(int i = 0; i < m[rig].length; i++) {
			m[rig][i]--;
		}
	}
}

class IncrementaColonna extends Thread {
	private int[][] m;
	private int col;
	
	public IncrementaColonna(int[][] m, int col) {
		this.m = m;
		this.col = col;
	}
	
	public void run() {
		for(int i = 0; i < m.length; i++) {
			m[i][col]++;
		}
	}
}

public class OperazioniAtomiche {
	public static void main(String[] args) {
		int[][] m = new int[100][100];
		
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[0].length; j++) {
				m[i][j] = 0;
			}
		}
		
		DecrementaRiga[] riga = new DecrementaRiga[m.length];
		IncrementaColonna[] colonna = new IncrementaColonna[m[0].length];
		
		for(int i = 0; i < m.length; i++) {
			riga[i] = new DecrementaRiga(m, i);
			riga[i].start();
		}
		
		for(int j = 0; j < m[0].length; j++) {
			colonna[j] = new IncrementaColonna(m, j);
			colonna[j].start();
		}
		
		stampaMatrice(m);
		
	}
	
	private static void stampaMatrice(int[][] m) {
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[0].length; j++) {
				System.out.print(m[i][j] + " ");
			}
			System.out.println();
		}
	}
}//

/**
	Non svolgo l'esercizio con gli AtomicInteger per la facilità di implementazione
	inoltre se si esegue questa classe in ambiente GNU/Linux la matrice non viene 
	affetta da race condition, in quanto in questo ambiente gli interi vengono incrementati
	decrementati atomicamente.
*/
