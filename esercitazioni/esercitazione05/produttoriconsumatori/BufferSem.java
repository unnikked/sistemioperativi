package esercitazioni.esercitazione05.produttoriconsumatori;

import java.util.concurrent.Semaphore;

public class BufferSem extends Buffer {
	//inizializzato a 0 significa
	//che il buffer è vuoto per cui
	//dobbiamo bloccare i consumatori
	private Semaphore ciSonoElementi = new Semaphore(0); 	
	
	private Semaphore ciSonoPostiVuoti;
	
	//con questo semaforo garantiamo 
	//la mutua esclusione durante 
	//l'inserimento e la rimozione
	//di elementi nel buffer
	private Semaphore mutex = new Semaphore(1); 
	
	public BufferSem(int dimensione) {
		super(dimensione);
		//lo inizializiamo a dimensione per far in modo
		//di bloccare i produttori quando il buffer è
		//pieno cioè quando i permessi disponibili 
		//del semaforo vanno a 0 che equivale al 
		//buffer pieno
		ciSonoPostiVuoti = new Semaphore(dimensione);
	}
	
	public void put(Elemento e) throws InterruptedException {
		//un produttore si sospende qui quando il
		//buffer è pieno 
		ciSonoPostiVuoti.acquire(); 
		mutex.acquire();
		buffer[in] = e;
		in = (in + 1) % buffer.length;
		mutex.release();
		//diciamo a tutti i thread sospesi su 
		//ciSonoElementi.acquire() che è 
		//disponibile un nuovo elemento
		ciSonoElementi.release(); 
	}
	
	public Elemento get() throws InterruptedException {
		//un consumatore si sospende qui quando
		//il buffer è vuoto
		ciSonoElementi.acquire(); 
		mutex.acquire();
		Elemento e = buffer[out];
		out = (out + 1) % buffer.length;
		mutex.release();
		//segnaliamo agli eventuali produttori che
		//un elemento è stato consumato, per cui 
		//possono tornare in produzione
		ciSonoPostiVuoti.release();
		return e;
	}
	public static void main(String[] args) {
		int dimensione = 10;
		Buffer buffer = new BufferSem(dimensione);
		int numProduttori = 10;
		int numConsumatori = 10;
		buffer.test(numProduttori, numConsumatori);
	}
}
