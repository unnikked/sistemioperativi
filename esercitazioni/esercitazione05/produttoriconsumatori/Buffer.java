package esercitazioni.esercitazione05.produttoriconsumatori;

/**
	> Il problema del produttore-consumatore <
	
	Conosciuto anche con il nome di problema del
	buffer-limitato (in inglese bounded-buffer, o
	producer-consumer), consiste in:
		● due tipi di processi, produttore e consumatore,
		che condividono un buffer a dimensione limitata
		● il compito del produttore è quello di generare
		ciclicamente dei dati e metterli nel buffer, quello
		del consumatore di rimuovere ciclicamente i dati
		dal buffer, uno alla volta
		● bisogna garantire che il produttore non inserisca
		dati nel buffer quando questo è pieno e che il
		consumatore non li prelevi quando questo è vuoto
		
	L'operazione di inserimento di dati nel buffer viene di
	solito chiamata put, mentre quella di prelievo get
		● Un produttore che invochi la put quando il buffer è
		pieno deve essere bloccato
		● Un consumatore che invochi la get quando il buffer è
		vuoto deve essere bloccato
		● Una variante del problema è quello in cui il buffer è
		illimitato, nel qual caso solo la get è bloccante
		mentre la put no
		
*/

public abstract class Buffer {
	protected Elemento[] buffer;
	protected int in = 0;
	protected int out = 0;
	
	public Buffer(int dimensione) {
		buffer = new Elemento[dimensione];
	}
	
	public abstract void put(Elemento e) throws InterruptedException;
	
	public abstract Elemento get() throws InterruptedException;
	
	public void test(int numProduttori, int numConsumatori) {
		for (int i = 0; i < numProduttori; i++) {
			new Thread(new Produttore(this)).start();
		}
		for (int i = 0; i < numConsumatori; i++) {
			new Thread(new Consumatore(this)).start();
		}
	}
}
