package esercitazioni.esercitazione05.produttoriconsumatori;

public class Elemento {
	private int valore;
	
	public Elemento(int val) {
		valore = val;
	}
	
	public int getValore() {
		return valore;
	}
	
	public String toString() {
		return "" + valore;
	}
}
