package esercitazioni.esercitazione05;

/**
	Scrivere una soluzione con i semafori della
	variante del problema del
	produttore-consumatore, con il buffer
	illimitato
*/

import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import esercitazioni.esercitazione05.produttoriconsumatori.Buffer;
import esercitazioni.esercitazione05.produttoriconsumatori.Elemento;
import esercitazioni.esercitazione05.produttoriconsumatori.Produttore;
import esercitazioni.esercitazione05.produttoriconsumatori.Consumatore;

public class BufferSemIllimitato extends Buffer {
	protected LinkedList<Elemento> bufferIllimitato;
	protected Semaphore mutex = new Semaphore(1);
	protected Semaphore ciSonoElementi = new Semaphore(0);
	
	public BufferSemIllimitato() {
		super(0);
		bufferIllimitato = new LinkedList<Elemento>();
	}
	
	public void put(Elemento e) throws InterruptedException {
		mutex.acquire();
		bufferIllimitato.addLast(e);
		System.out.println("Produttore " + Thread.currentThread().getId() + " produce " + e.getValore() + "\t\t" + bufferIllimitato.toString());
		mutex.release();
		ciSonoElementi.release();
	}
	
	public Elemento get() throws InterruptedException {
		ciSonoElementi.acquire();
		mutex.acquire();
		Elemento e = bufferIllimitato.removeFirst();
		System.out.println("Consumatore " + Thread.currentThread().getId() + " consuma " + e.getValore() + "\t" + bufferIllimitato.toString());
		mutex.release();
		return e;
	}
	
	public static void main(String[] args) {
		int numeroProduttori = 10;
		int numeroConsumatori = 50;
		new BufferSemIllimitato().test(numeroProduttori, numeroConsumatori);
	}
}
