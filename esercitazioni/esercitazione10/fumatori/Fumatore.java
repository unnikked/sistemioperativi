package esercitazioni.esercitazione10.fumatori;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Fumatore implements Runnable {
	private Tavolo tavolo;
	private int ingredientePosseduto;
	private static int MIN_SLEEP = 3;
	private static int MAX_SLEEP = 5;
	
	public Fumatore(Tavolo t, int tipo) {
		tavolo = t;
		ingredientePosseduto = tipo;
	}
	
	public void run() {
		try {
			while (true) {
				tavolo.prendi(ingredientePosseduto);
				preparaSigarettaEFuma();
			}
		} catch (InterruptedException e) {
			//
		}
	}
	
	private void preparaSigarettaEFuma() throws InterruptedException {
		TimeUnit.SECONDS.sleep(new Random().nextInt(MAX_SLEEP - MIN_SLEEP + 1) + MIN_SLEEP);
	}
}

