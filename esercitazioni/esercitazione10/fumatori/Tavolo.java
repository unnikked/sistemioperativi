package esercitazioni.esercitazione10.fumatori;

/**
	Ideato da Suhas Patil nel 1971
	Per fare una sigaretta sono necessari tre ingredienti: il
	tabacco, la carta e un fiammifero
	Vi sono tre fumatori, ognuno dei quali possiede uno dei
	tre ingredienti in quantità infinita, e un tabaccaio che
	possiede tutti gli ingredienti in quantità infinita
	Il tabaccaio sceglie due ingredienti a caso e li mette a
	disposizione del fumatore che non li possiede
	Ogni fumatore esegue ripetutamente un ciclo in cui
	prende i due ingredienti, prepara una sigaretta e poi la
	fuma
	Non appena gli ingredienti sono stati prelevati il
	tabaccaio ne sceglie altri due a caso e li mette a
	disposizione
*/

public abstract class Tavolo {
	/**
		La soluzione che segue è parametrizzata sul numero di ingredienti,
		ovvero volendo risolvere il problema dei fumatori con un numero
		di ingredienti diverso da 3, l'unico punto in tutto il codice che dovremo
		modificare sarà solo il valore di questa costante
	*/
	public static final int NUM_INGREDIENTI = 3;
	protected int[] ingredienti = new int[NUM_INGREDIENTI - 1];
	
	public abstract void prendi(int ingrediente) throws InterruptedException;
	
	public abstract void metti(int[] ingredienti) throws InterruptedException;
	
	public void test() {
		new Thread(new Tabaccaio(this)).start();
		new Thread(new Fumatore(this, 0)).start();
		new Thread(new Fumatore(this, 1)).start();
		new Thread(new Fumatore(this, 2)).start();
	}
	
}
