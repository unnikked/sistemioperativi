package esercitazioni.esercitazione09;

/**
	Risolvere il problema del barbiere
	addormentato imponendo una politica FIFO di
	richiamo dei clienti dalla sala d'attesa: scrivere
	una soluzione con i semafori
*/

import esercitazioni.esercitazione09.barbiereaddormentato.*;
import java.util.concurrent.Semaphore;

public class SaloneSemFIFO extends Salone {
	private Semaphore mutex 			= new Semaphore(1);
	private Semaphore barbiere 			= new Semaphore(0);
	private Semaphore clienteServito 	= new Semaphore(0);
	/*
		Per garantire il risveglio in ordine FIFO
		impostiamo la fairness a true sul semaforo
		dei clieni in attesa
	*/
	private Semaphore clienteInAttesa 	= new Semaphore(0, true);
	private long idCliente;

	public SaloneSemFIFO(int c) {
		super(c);
	}
	
	public boolean entra() throws InterruptedException {
		boolean servito = true;
		mutex.acquire();
		System.out.println("Il Cliente#" + Thread.currentThread().getId() + " entra nel salone");
		if (barbiereAddormentato) {
			siediInPoltrona();
		} else if (numClientiInAttesa < capienza) { 
			attendiInSalaDAttesa();
			siediInPoltrona();
		} else {
			servito = false;
			System.out.println("Il cliente#" + Thread.currentThread().getId() + " non viene servito, clienti in attesa " + numClientiInAttesa);
		}
		mutex.release();
		return servito;
	}
	
	private void attendiInSalaDAttesa() throws InterruptedException {
		numClientiInAttesa++;
		System.out.println("Il cliente#" + Thread.currentThread().getId() + " si mette in attesa, clienti in attesa " + numClientiInAttesa);
		mutex.release();
		clienteInAttesa.acquire();
		numClientiInAttesa--;
	}
	
	private void siediInPoltrona() throws InterruptedException {
		barbiere.release();
		idCliente = Thread.currentThread().getId();
		System.out.println("Il cliente#" + idCliente + " si siede in poltrona, clienti in attesa " + numClientiInAttesa);
		clienteServito.acquire();
	}
	
	public void serviCliente() throws InterruptedException {
		mutex.acquire();
		if (numClientiInAttesa == 0) {
			barbiereAddormentato = true;
			System.out.println("Il barbiere si addormenta, clienti in attesa " + numClientiInAttesa);
			mutex.release();
			barbiere.acquire(); 
			barbiereAddormentato = false;
		} else {
			clienteInAttesa.release();
			System.out.println("Il barbiere serve il cliente#" + idCliente + ", clienti in attesa " + numClientiInAttesa);
			barbiere.acquire(); 
		}
		mutex.release();
	}

	public void congedaCliente() throws InterruptedException {
		mutex.acquire();
		clienteServito.release(); 
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SaloneSemFIFO(5).test(50);
	}
}
