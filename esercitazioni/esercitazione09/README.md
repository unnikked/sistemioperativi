# Il problema del barbiere addormentato
Ideato da Edsger Dijkstra nel 1965.

Un barbiere ha una poltrona da
lavoro e una sala d'attesa con un
numero limitato di posti

Ogni cliente quando arriva
controlla cosa sta facendo il
barbiere:
* se sta dormendo, lo sveglia e si
siede sulla poltrona per essere
servito
* se sta servendo un altro cliente va
ad aspettare nella sala d'attesa:
se questa è piena, va via

Il barbiere serve il cliente
seduto sulla poltrona:
* quando finisce di servire il
cliente, lo congeda e serve il
cliente successivo
richiamandolo dalla sala
d'attesa
* se nella sala d'attesa non c'è
nessuno il barbiere si mette a
dormire sulla poltrona
