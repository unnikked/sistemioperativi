package esercitazioni.esercitazione09.barbiereaddormentato;

public class SaloneSync extends Salone {
	private boolean clienteServito 		= false;
	private boolean prossimoCliente 	= false;
	
	public SaloneSync(int c) {
		super(c);
	}
	
	public synchronized boolean entra() throws InterruptedException {
		boolean servito = true;
		if (barbiereAddormentato && !prossimoCliente) {
			siediInPoltrona();
		} else if (numClientiInAttesa < capienza) {
			attendiInSalaDAttesa();
			siediInPoltrona();
		} else {
			servito = false;
		}
		return servito;
	}

	private void attendiInSalaDAttesa() throws InterruptedException {
		numClientiInAttesa++;
		while (!prossimoCliente) {
			wait();
		}
		prossimoCliente = false;
		numClientiInAttesa--;
	}
	
	private void siediInPoltrona() throws InterruptedException {
		barbiereAddormentato = false;
		/**
			Il barbiere ed eventuali clienti nella sala
			d'attesa sono sospesi sulla stessa condition:
			per avere la certezza di svegliare il barbiere
			sono costretto a svegliarli tutti.
			
		*/
		notifyAll();
		
		while (!clienteServito) {
			wait();
		}
		clienteServito = false;
		notifyAll();
	}
	
	public synchronized void serviCliente() throws InterruptedException {
		if (numClientiInAttesa > 0) {
			prossimoCliente = true;
			/* 
				Il barbiere sono io, sulla poltrona non c'è
				nessun cliente, quindi nella condition sono
				sospesi solo clienti nella sala d'attesa.
			*/
			notify();
		}
		barbiereAddormentato = true;
		while (barbiereAddormentato) {
			wait();
		}
	}
	
	public synchronized void congedaCliente() throws InterruptedException {
		clienteServito = true;
		/*
			Il cliente in poltrona ed eventuali clienti nella
			sala d'attesa sono sospesi sulla stessa condition:
			per avere la certezza di svegliare il cliente in
			poltrona sono costretto a svegliarli tutti.
		*/
		notifyAll();
		while (clienteServito) {
			wait();	
		}
	}
	
	public static void main(String[] args) {
		System.exit(0);
	}
}
