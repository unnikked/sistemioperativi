package esercitazioni.esercitazione09.barbiereaddormentato;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Barbiere implements Runnable {
	private Salone salone;
	private static int MIN_SLEEP = 1;
	private static int MAX_SLEEP = 2;
	
	public Barbiere(Salone s) {
		salone = s;
	}
	
	public void run() {
		try {
			while (true) {
				salone.serviCliente();
				tagliaCapelli();
				salone.congedaCliente();
			}
		} catch (InterruptedException e) {
		
		}
	}
	
	private void tagliaCapelli() throws InterruptedException {
		TimeUnit.SECONDS.sleep(new Random().nextInt(MAX_SLEEP - MIN_SLEEP + 1) + MIN_SLEEP);
	}
}
