package esercitazioni.esercitazione09.barbiereaddormentato;

import java.util.concurrent.Semaphore;

public class SaloneSem extends Salone {
	private Semaphore mutex 			= new Semaphore(1);
	private Semaphore barbiere 			= new Semaphore(0);
	private Semaphore clienteServito 	= new Semaphore(0);
	private Semaphore clienteInAttesa 	= new Semaphore(0);
	private long idCliente; //

	public SaloneSem(int c) {
		super(c);
	}
	
	public boolean entra() throws InterruptedException {
		boolean servito = true;
		mutex.acquire();
		/*
			Nello stato iniziale, se il primo
			cliente entra nel salone prima
			del barbiere, il cliente andrà ad
			aspettare nella sala d'attesa
		*/
		System.out.println("Il Cliente#" + Thread.currentThread().getId() + " entra nel salone");
		if (barbiereAddormentato) {
			siediInPoltrona(); //se il barbiere è addormentato il cliente
							   //lo sveglia e si siede sulla poltrona
		} else if (numClientiInAttesa < capienza) { 
			attendiInSalaDAttesa();
			siediInPoltrona();
		} else {
			servito = false;
			System.out.println("Il cliente#" + Thread.currentThread().getId() + " non viene servito, clienti in attesa " + numClientiInAttesa);
		}
		mutex.release();
		return servito;
	}
	
	private void attendiInSalaDAttesa() throws InterruptedException {
		numClientiInAttesa++;
		System.out.println("Il cliente#" + Thread.currentThread().getId() + " si mette in attesa, clienti in attesa " + numClientiInAttesa);
		mutex.release();
		clienteInAttesa.acquire();
		numClientiInAttesa--;
	}
	
	private void siediInPoltrona() throws InterruptedException {
		barbiere.release(); // Passaggio implicito della mutua esclusione - sblocco/sveglio il barbiere
		idCliente = Thread.currentThread().getId(); //per debugging 
		System.out.println("Il cliente#" + idCliente + " si siede in poltrona, clienti in attesa " + numClientiInAttesa);
		clienteServito.acquire(); //blocco eventuali clienti che vorrebbero sedersi sulla poltrona
	}
	
	public void serviCliente() throws InterruptedException {
		mutex.acquire();
		if (numClientiInAttesa == 0) {
			barbiereAddormentato = true;
			System.out.println("Il barbiere si addormenta, clienti in attesa " + numClientiInAttesa);
			mutex.release();
			barbiere.acquire(); //sospendo il barbiere, non ci sono clienti quando un cliente lo
								//risveglia mette la varibile a false
			barbiereAddormentato = false;
		} else {
			clienteInAttesa.release(); // Passaggio implicito della mutua esclusione
			System.out.println("Il barbiere serve il cliente#" + idCliente + ", clienti in attesa " + numClientiInAttesa);
			barbiere.acquire(); // Aspetto che il cliente si sieda sulla poltrona
		}
		mutex.release();
	}

	public void congedaCliente() throws InterruptedException {
		mutex.acquire();
		clienteServito.release(); // Passaggio implicito della mutua esclusione
		// è disponibile la sedie per un altro taglio
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SaloneSem(5).test(50);
	}
}
