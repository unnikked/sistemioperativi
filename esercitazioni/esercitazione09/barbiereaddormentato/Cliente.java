package esercitazioni.esercitazione09.barbiereaddormentato;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Cliente implements Runnable {
	private Salone salone;
	private static int MIN_SLEEP = 3;
	private static int MAX_SLEEP = 5;
	
	public Cliente(Salone s) {
		salone = s;
	}
	
	public void run() {
		try {
			raggiungiSalone();
			if (salone.entra()) {
				//System.out.println("Cliente#" + Thread.currentThread().getId() + " servito");
				System.out.print("");
				
			} else {
				//System.out.println("Cliente#" + Thread.currentThread().getId() + " non è stato servito");
				System.out.print("");
			}
		} catch (InterruptedException e) {
		
		}
	}
	
	private void raggiungiSalone() throws InterruptedException {
		TimeUnit.SECONDS.sleep(new Random().nextInt(MAX_SLEEP - MIN_SLEEP + 1) + MIN_SLEEP);
	}
}
