package esercitazioni.esercitazione09.barbiereaddormentato;

public abstract class Salone {
	protected int capienza;
	protected int numClientiInAttesa = 0;
	/*
		Nello stato iniziale il barbiere
		non è ancora arrivato nel salone
	*/
	protected boolean barbiereAddormentato = false;
	
	public Salone(int c) {
		capienza = c;
	}
	
	protected abstract void serviCliente() throws InterruptedException;
	protected abstract void congedaCliente() throws InterruptedException;
	protected abstract boolean entra() throws InterruptedException;
	
	public void test(int numClienti) throws InterruptedException {
		//new Thread(new Barbiere(this)).start();
		for(int i = 0; i < numClienti; i++) {
			new Thread(new Cliente(this)).start();
		}
		new Thread(new Barbiere(this)).start();
	}
}
