package esercitazioni.esercitazione09.barbiereaddormentato;

import java.util.concurrent.locks.*;

public class SaloneLC extends Salone {
	private Lock l 						= new ReentrantLock();
	private Condition barbiere 			= l.newCondition();
	private Condition salaDAttesa 		= l.newCondition();
	private Condition taglioCapelli		= l.newCondition();
	private boolean clienteServito 		= false;
	private boolean prossimoCliente 	= false;

	public SaloneLC(int c) {
		super(c);
	}
	
	public boolean entra() throws InterruptedException {
		boolean servito = true;
		l.lock();
		try {
			/*
				Il cliente che entra da fuori si siede
				solo se non è stato già chiamato
				un cliente dalla sala d'attesa.
				Questo controllo è necessario
				nello scenario in cui un cliente
				che arriva da fuori acquisisca il
				lucchetto prima del cliente
				risvegliato dalla sala d'attesa.
			*/
			if (barbiereAddormentato && !prossimoCliente) {
				siediInPoltrona();
			} else if (numClientiInAttesa < capienza) {
				attendiInSalaDAttesa();
				siediInPoltrona();
			} else {
				servito = false;
			}
			
		} finally {
			l.unlock();
		}
		return servito;	
	}
	
	private void attendiInSalaDAttesa() throws InterruptedException {
		numClientiInAttesa++;
		while (!prossimoCliente) {
			salaDAttesa.await();
		}
		prossimoCliente = false;
		numClientiInAttesa--;
	}
	
	private void siediInPoltrona() throws InterruptedException {
		barbiereAddormentato = false;
		barbiere.signal();
		while (!clienteServito) {
			taglioCapelli.await();
		}
		clienteServito = false;
		barbiere.signal();
		// Risveglio il barbiere che sta
		// aspettando che io lasci la poltrona
	}
	
	public void serviCliente() throws InterruptedException {
		l.lock();
		try {
			if(numClientiInAttesa > 0) {
				prossimoCliente = true;
				salaDAttesa.signal();
			}
			barbiereAddormentato = true;
			while(barbiereAddormentato) {
				barbiere.await();
				// Aspetto che un cliente
				// si sieda sulla poltrona
			}
		} finally {
			l.unlock();
		}
	}
	
	public void congedaCliente() throws InterruptedException {
		l.lock();
		try {
			clienteServito = true;
			taglioCapelli.signal();
			while(clienteServito) {
				barbiere.await();
				// Aspetto che il cliente
				//lasci la poltrona
			}
		} finally {
			l.unlock();
		}
	}
	
	public static void main(String[] args) {
		//new SaloneLC(5).test(20);
	}
}
	
