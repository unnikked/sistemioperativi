package esercitazioni.esercitazione09;

/**
	Risolvere il problema del barbiere
	addormentato imponendo una politica FIFO di
	richiamo dei clienti dalla sala d'attesa: scrivere
	una soluzione con i lock e condition
*/

import esercitazioni.esercitazione09.barbiereaddormentato.*;
import java.util.LinkedList;
import java.util.concurrent.locks.*;

public class SaloneLCFIFO extends Salone {
	private LinkedList<Thread> codaAttesa 	= new LinkedList<Thread>();
	private Lock l 							= new ReentrantLock();
	private Condition barbiere 				= l.newCondition();
	private Condition salaDAttesa 			= l.newCondition();
	private Condition taglioCapelli			= l.newCondition();
	private boolean clienteServito 			= false;
	private boolean prossimoCliente 		= false;

	public SaloneLCFIFO(int c) {
		super(c);
	}
	
	public boolean entra() throws InterruptedException {
		boolean servito = true;
		l.lock();
		try {
			/*
				Il cliente che entra da fuori si siede
				solo se non è stato già chiamato
				un cliente dalla sala d'attesa.
				Questo controllo è necessario
				nello scenario in cui un cliente
				che arriva da fuori acquisisca il
				lucchetto prima del cliente
				risvegliato dalla sala d'attesa.
			*/
			if (barbiereAddormentato && !prossimoCliente) {
				siediInPoltrona();
			} else if (numClientiInAttesa < capienza) {
				attendiInSalaDAttesa();
				siediInPoltrona();
			} else {
				servito = false;
			}
			
		} finally {
			l.unlock();
		}
		return servito;	
	}
	
	private void attendiInSalaDAttesa() throws InterruptedException {
		numClientiInAttesa++;
		codaAttesa.addLast(Thread.currentThread());
		while (!prossimoCliente || codaAttesa.getFirst() != Thread.currentThread()) {
			salaDAttesa.await();
		}
		prossimoCliente = false;
		codaAttesa.removeFirst();
		numClientiInAttesa--;
	}
	
	private void siediInPoltrona() throws InterruptedException {		
		barbiereAddormentato = false;
		barbiere.signal();
		while (!clienteServito) {
			taglioCapelli.await();
		}
		clienteServito = false;
		barbiere.signal();
	}
	
	public void serviCliente() throws InterruptedException {
		l.lock();
		try {
			if(numClientiInAttesa > 0) {
				prossimoCliente = true;
				salaDAttesa.signal();
			}
			barbiereAddormentato = true;
			while(barbiereAddormentato) {
				barbiere.await();
				// Aspetto che un cliente
				// si sieda sulla poltrona
			}
		} finally {
			l.unlock();
		}
	}
	
	public void congedaCliente() throws InterruptedException {
		l.lock();
		try {
			clienteServito = true;
			taglioCapelli.signal();
			while(clienteServito) {
				barbiere.await();
				// Aspetto che il cliente
				//lasci la poltrona
			}
		} finally {
			l.unlock();
		}
	}
	
	public static void main(String[] args) {
		//new SaloneLC(5).test(20);
	}
}
