package esercitazioni.esercitazione08.lettoriscrittori;

/**
	Implementazione del problema lettori scrittori
	del terzo tipo tramite monitor nativi di Java
*/

import esercitazioni.esercitazione04.lettoriscrittori.*;
import java.util.*;

public class MemoriaCondivisa3FIFOSync extends MemoriaCondivisa {
	private int numLettoriInLettura = 0;
	private boolean scrittoreInScrittura = false;
	private LinkedList<Thread> codaThread =	new LinkedList<Thread>();
	
	public synchronized void inizioLettura() throws InterruptedException {
		codaThread.add(Thread.currentThread());
		while (!possoLeggere()) {
			wait();
		}
		codaThread.removeFirst();
		numLettoriInLettura++;
	}
	
	public synchronized void fineLettura() throws InterruptedException {
		numLettoriInLettura--;
		if (!codaThread.isEmpty()) {
			notifyAll();
		}
	}
	
	private boolean possoLeggere() {
		return !scrittoreInScrittura
			&& codaThread.getFirst() == Thread.currentThread();
	}
	
	public synchronized void inizioScrittura() throws InterruptedException {
		codaThread.add(Thread.currentThread());
		while (!possoScrivere()) {
			wait();
		}
		codaThread.removeFirst();
		scrittoreInScrittura = true;
	}
	
	public synchronized void fineScrittura() throws InterruptedException {
		scrittoreInScrittura = false;
		if (!codaThread.isEmpty()) {
			notifyAll();
		}
	}
	
	private boolean possoScrivere() {
		return numLettoriInLettura == 0 && possoLeggere();
	}
}

/**
	Problema: nello scenario in cui si trovino più
	thread lettori e scrittori in attesa, può
	succedere che un lettore (L2 o L3) che
	potrebbe leggere (perché prima di lui non si
	trova alcuno scrittore in attesa) torni a
	sospendersi (perché non è il primo della lista
	codaThread) nel caso riesca ad acquisire il
	lucchetto, a seguito della notifyAll, prima dei
	lettori che lo precedono.
	
	Soluzione: “allentiamo” il vincolo FIFO
	garentendolo solo per i tipi, non più per i
	singoli thread, e modifichiamo
	opportunamente la condizione nel metodo
	possoLeggere
*/
