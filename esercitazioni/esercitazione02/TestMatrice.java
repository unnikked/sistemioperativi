package esercitazioni.esercitazione02;

/**
	Si realizzi un’applicazione multithreaded che, data una
	matrice rettangolare di interi, individua, se esiste, un
	elemento che è contemporaneamente il massimo della
	propria riga ed il minimo della propria colonna.
	
	In particolare, data una matrice n x m, si utilizzino:
	
	  ● n thread, ciascuno dei quali calcola il massimo di una
		determinata riga della matrice;
	  ● m thread, ciascuno dei quali calcola il minimo di una
		determinata colonna della matrice.
	
	Si definisca un main che inizializza una matrice di test e,
	utilizzando opportunamente gli oggetti thread, individua e
	stampa la posizione di un elemento della matrice che è
	massimo della propria riga e minimo della propria colonna.
*/

class MassimoRiga extends Thread {
	private int[][] matrix;
	private	int row;
	private int max = Integer.MIN_VALUE;
	
	public MassimoRiga(int[][] matrix, int row) {
		this.matrix = matrix;
		this.row = row;
	}//
	
	public int getMassimo() {
		try {
			join();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return max;
	}//
	
	public void run() {
		for(int i = 0; i < matrix[0].length; i++) {
			if(max < matrix[row][i]) {
				max = matrix[row][i];
			}
		}
	}//
}//MassimoRiga

class MinimoColonna extends Thread {
	private int[][] matrix;
	private int col;
	private int min = Integer.MAX_VALUE;
	
	public MinimoColonna(int[][] matrix, int col) {
		this.matrix = matrix;
		this.col = col;
	}//
	
	public int getMinimo() {
		return min;
	}//
	
	public void run() {
		for(int i = 0; i < matrix.length; i++) {
			if(min > matrix[i][col]) {
				min = matrix[i][col];
			}
		}
	}
}//MinimoColonna

public class TestMatrice {
	public static void main(String[] args) {
		int[][] matrice = { {3, 7, 3, 2},
							{9, 8, -2, -1},
							{9, 12, 9, 15}
						  };
		MassimoRiga[] righe = new MassimoRiga[matrice.length];
		MinimoColonna[] colonne = new MinimoColonna[matrice[0].length];
		
		for(int i = 0; i < righe.length; i++) {
			righe[i] = new MassimoRiga(matrice, i);
			righe[i].start();
		}
		
		for(int i = 0; i < colonne.length; i++) {
			colonne[i] = new MinimoColonna(matrice, i);
			colonne[i].start();
		}
		
		for(int i = 0; i < righe.length; i++) {
			for(int j = 0; j < colonne.length; j++) {
				if(righe[i].getMassimo() == colonne[j].getMinimo()) {
					System.out.println("Valore " + matrice[i][j] + ", pos (" + i + "; " + j + ")");
				}
			}
		}

	}
}
