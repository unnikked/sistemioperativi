package esercitazioni.esercitazione02;

/**
	Si realizzi un’applicazione multithreaded che calcola il
	prodotto scalare di due array di interi di dimensione n.
	
	Il sistema utilizza m oggetti thread istanza di una classe
	ProdottoScalare. Ciascun oggetto ProdottoScalare calcola
	il prodotto scalare su una porzione di lunghezza n/m dei
	due array (si assuma che n sia multiplo di m).
	
	Si definisca un main che inizializzi due array, e utilizzando
	opportunamente gli oggetti thread, calcoli e stampi il
	prodotto scalare dei due array.
	Si assuma che m sia una costante definita nel programma..
*/

class ProdottoScalare extends Thread {
	private int da;
	private int a;
	private int[] V1;
	private int[] V2;
	private int prodottoParziale;
	
	public ProdottoScalare(int da, int a, int[] V1, int[]V2) {
		this.da = da;
		this.a = a;
		this.V1 = V1;
		this.V2 = V2;
		this.prodottoParziale = 0;
	}//
	
	public void run() {
		System.out.println("Thread id " + getId() + " started");
		for(int i = da; i < a; i++) {
			prodottoParziale += V1[i] * V2[i]; 
		}
	}//
	
	public int getProdottoParziale() throws InterruptedException {
		join();
		return prodottoParziale;
	}
	
	public static void main(String[] args) {
		int[] V1 = {5, 4, 3, 7, 8, 4, 1, 9};
		int[] V2 = {4, 3, 7, 9, 2, 3, 4, 4};
		final int M = 4;
		int sezione = V1.length / M;
		ProdottoScalare[] prodottiParziali = new ProdottoScalare[M];
		for(int i = 0; i < M; i++) {
			prodottiParziali[i] = new ProdottoScalare(i * sezione, (i + 1) * sezione, V1, V2);
			prodottiParziali[i].start();
		}
		int prodotto = 0;
		for(int i = 0; i < M; i++) {
			try {
				prodotto += prodottiParziali[i].getProdottoParziale();
			}catch(InterruptedException e) {
				//
			}
		}
		System.out.println("Prodotto Scalare = " + prodotto);
	}
}
