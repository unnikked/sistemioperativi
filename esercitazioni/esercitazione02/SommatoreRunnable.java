package esercitazioni.esercitazione02;

/**
	Riscrivere il thread Sommatore in modo che
	implementi l'interfaccia Runnable.
*/

class Sommatore implements Runnable {
	private int da;
	private int a;
	private int somma;
	
	public Sommatore(int da, int a) {
		this.da = da;
		this.a = a;
	}//
	
	public int getSomma() {
		return somma; 
	}//
	
	public void run() {
		somma = 0;
		for (int i = da; i <= a; i++) {
			somma += i;
		}
	}//
}

public class SommatoreRunnable {
	public static void main(String[] args) {
		int primo = 1;
		int ultimo = 100;
		int intermedio = (ultimo + primo) / 2;
		
		Sommatore sommaA = new Sommatore(primo, intermedio);
		Sommatore sommaB = new Sommatore(intermedio + 1, ultimo);
		
		Thread sommatoreA = new Thread(sommaA);
		Thread sommatoreB = new Thread(sommaB);

		sommatoreA.start();
		sommatoreB.start();
		
		try {
			sommatoreA.join();
			sommatoreB.join();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(sommaA.getSomma() + sommaB.getSomma());
	}
}
