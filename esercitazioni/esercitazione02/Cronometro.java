//package esercitazioni.esercitazione02;

/*
	Usando il meccanismo per l'interruzione dei thread,
	modificare ed estendere la classe Cronometro (che deve
	implementare Runnable) e AzionatoreCronometro in modo
	che:
	
		● la pressione del tasto invio, quando il cronometro è in
		esecuzione, metta in pausa il cronometro e mostri il
		numero di secondi trascorsi dall'ultima pausa
		● la pressione del tasto invio, quando il cronometro è in
		pausa, faccia ripartire il cronometro
		● la digitazione della stringa “stop” seguita da invio
		termini l'orologio e mostri il numero di secondi trascorsi
		dall'avvio dell'orologio e il numero delle pause
		
*/
import java.util.*;
import java.util.concurrent.*;

class CronometroPausa implements Runnable {
	private boolean interrompi = false;
	private int numSecondi;
	private int numPause;

	public void run() {
		int ultimiSecondi = 0;
		while (!Thread.currentThread().isInterrupted()) {
			try {
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				if (interrompi) {
					System.out.println("Numero secondi trascorsi: \t"
							+ numSecondi);
					System.out.println("Numero pause:             \t"
							+ numPause);
					break;
				}
				System.out.println("Secondi trascorsi dall'ultima pausa: "
						+ ultimiSecondi);
				ultimiSecondi = 0;
				numPause++;
			}
			numSecondi++;
			ultimiSecondi++;
		}
	}

	public void interrompi() {
		interrompi = true;
		//Thread.currentThread().interrupt();
	}

	public boolean èInterrotto() {
		return interrompi;
	}
	
}

public class Cronometro {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		CronometroPausa cron = new CronometroPausa();
		Thread cronometro = new Thread(cron);
		System.out.println("Premi invio per iniziare");
		in.nextLine();
		cronometro.start();
		System.out
				.println("Premi invio per mettere in pausa o digita \"stop\" per fermare il cronometro");
		do {
			if (in.nextLine().equalsIgnoreCase("stop")) {
				cron.interrompi();
				cronometro.interrupt();
			} else
				cronometro.interrupt();
		} while (!cron.èInterrotto());

	}

}
