package esercitazioni.esercitazione02;

/**
	Si realizzi un’applicazione multithreaded che, data una
	matrice di interi A e due numeri interi x ed y, verifica se
	nella matrice A il numero di ripetizioni di x è
	strettamente maggiore del numero di ripetizioni di y.
	
	Se A è costituita da k righe, l’applicazione dovrà
	utilizzare k thread, ciascuno dei quali conterà le
	ripetizioni di x ed y in una determinata riga di A.
	
	Si definisca un main che inizializzi la matrice A e gli interi
	x ed y, conta il numero di ripetizioni di x ed y in A
	utilizzando un insieme di thread che operano in
	parallelo, ed infine stampa true se x è più presente di y,
	false altrimenti.
*/

class Verifica extends Thread {
	private	int x, y, rig;
	private	int[][] m;
	boolean risultato;
	
	public Verifica(int[][] m, int x, int y, int rig) {
		this.m = m;
		this.x = x;
		this.y = y;
		this.rig = rig;
	}
	
	public boolean getRisultato() {
		try {
			join();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return risultato;
	}
	
	public void run() {
		int countX = 0, countY = 0;
		for(int i = 0; i < m[0].length; i++) {
			if(x == m[rig][i]) {
				countX++;
			}
			if(y == m[rig][i]) {
				countY++;
			}
			risultato = countX > countY;
		}
	}
}

public class VerificaRipetizioni {
	public static void main(String[] args) {
		int[][] A = {{2,2,3,3,3,4},
					 {3,2,3,3,3,1},
					 {5,4,3,2,3,3}};
		int x = 3, y = 2;	
		Verifica[] v = new Verifica[A.length];
		for(int i = 0; i < A.length; i++) {
			v[i] = new Verifica(A, x, y, i);
			v[i].start();
		}
		for(int i = 0; i < v.length; i++) {
			if(!v[i].getRisultato()) {
				System.out.println("Falso");
				System.exit(1);
			}
		}
		System.out.println("Vero");
	}
}
