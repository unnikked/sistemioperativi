package esercitazioni.esercitazione07.produttoriconsumatori;

/**
	Variante del problema produttori-consumatori
	con l'uso delle Locks&Contitions
*/

import esercitazioni.esercitazione05.produttoriconsumatori.Buffer;
import esercitazioni.esercitazione05.produttoriconsumatori.Elemento;
import java.util.concurrent.locks.*;

public class BufferLC extends Buffer {
	protected int numElementi = 0;
	protected Lock l = new ReentrantLock(); 
	protected Condition bufferNonPieno = l.newCondition();
	protected Condition bufferNonVuoto = l.newCondition();
	
	public BufferLC(int dimensione) {
		super(dimensione);
	}
	
	public void put(Elemento i) throws InterruptedException {
		l.lock();
		try {
			/*
				Fintantoché il buffer è pieno i produttori
				non possono produrre, pertanto si sospendono
				sulla Condition bufferNonPieno
			*/
			while (numElementi == buffer.length) {
				bufferNonPieno.await();
			}
			buffer[in] = i;
			in = (in + 1) % buffer.length;
			numElementi++;
			/*
				Segnaliamo ai consumatori che un nuovo
				elemento è stato inserito per cui uno
				può prelevare tale elemento.
			*/
			bufferNonVuoto.signal();
		} finally {
			l.unlock();
		}
	}
	
	public Elemento get() throws InterruptedException {
		Elemento item = null;
		l.lock();
		try {
			/*
				Fintantoché il buffer è vuoto i consumatori
				aspettano che esso si riempia con un Elemento
			*/
			while(numElementi == 0) {
				bufferNonVuoto.await();
			}
			item = buffer[out];
			buffer[out] = null;
			out = (out + 1) % buffer.length;
			numElementi--;
			/*
				Segnaliamo ai produttori che è stato consumato
				un elemento.
			*/
			bufferNonPieno.signal();
		} finally {
			l.unlock();
		}
		return item;
	}
}
