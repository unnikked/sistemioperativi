package esercitazioni.esercitazione07.produttoriconsumatori;

/**
	Supponiamo di voler gestire i thread in attesa sulle
	condition con una certa politica
	Poiché la signal non risveglia necessariamente il
	Thread in attesa da più tempo, è necessario usare
	signalAll per risvegliare tutti i Thread
	Ogni Thread al proprio risveglio dovrà controllare se
	è lui quello che ha diritto a riprendere l’esecuzione;
	se così non fosse si rimette in attesa sulla stessa
	condition
*/

import esercitazioni.esercitazione05.produttoriconsumatori.Elemento;
import java.util.LinkedList;
import java.util.concurrent.locks.*;

public class BufferLCFifo extends BufferLC {
	private LinkedList<Thread> codaProduttori =	new LinkedList<Thread>();
	private LinkedList<Thread> codaConsumatori = new LinkedList<Thread>();
	
	public BufferLCFifo(int dimensione) {
		super(dimensione);
	}
	
	public void put(Elemento i) throws InterruptedException {
		l.lock();
		try {
			codaProduttori.add(Thread.currentThread());
			while (!possoInserire()) {
				bufferNonPieno.await();
			}
			codaProduttori.removeFirst();
			buffer[in] = i;
			in = ((in + 1) % buffer.length);
			numElementi++;
			bufferNonVuoto.signalAll();
		} finally {
			l.unlock();
		}
	}
	
	private boolean possoInserire() {
		return numElementi < buffer.length && codaProduttori.getFirst() == Thread.currentThread();
	}
	
	public Elemento get() throws InterruptedException {
		Elemento i = null;
		l.lock();
		try {
			codaConsumatori.add(Thread.currentThread());
			while (!possoPrelevare()) {
				bufferNonVuoto.await();
			}
			codaConsumatori.removeFirst();
			i = buffer[out];
			out = ((out + 1) % buffer.length);
			numElementi--;
			bufferNonPieno.signalAll();
		} finally {
			l.unlock();
		}
		return i;
	}
	
	private boolean possoPrelevare() {
		return numElementi > 0 && codaConsumatori.getFirst() == Thread.currentThread();
	}
}
	
