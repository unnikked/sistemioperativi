package poo.thread.scambiatore;

public class ExchangerMJ<T> implements Exchanger<T>{
	private T primo, secondo;
	private boolean partner = false, rilascio = false;
	private Object lock = new Object();
	public T exchange( T msg ){
		synchronized( lock ){
			while( rilascio )//per rientro "veloce"
				try{ lock.wait(); }
				catch(InterruptedException e){}
			if( partner ){
				secondo = msg;
				T t = primo;
				partner = false;
				rilascio = true;
				lock.notify();
				return t;
			}
			//partner==false
			primo = msg;
			partner = true;
			while( partner )
				try{ lock.wait(); }catch(InterruptedException e){}
			T t = secondo;
			rilascio = false;
			lock.notify();
			return t;
		}
	}
}//ExchangerMJ
