package appelli.piscina;

/**
	Ogni persona impiega da 3 a 5 minuti per
	cambiarsi, poi raggiunge la piscina ed entra nella corsia con il minor numero di persone: se tutte le corsie
	sono al completo attende in una coda FIFO che si liberi un posto in una corsia. Quando riesce ad entrare in
	una corsia la impegna per 45 minuti, poi libera la corsia, quindi si fa la doccia, impiegando per questo da 10
	a 15 minuti, dopodiché va via.
*/

import java.util.*;
import java.util.concurrent.*;

public class PiscinaSem extends Piscina {
	private Semaphore mutex = new Semaphore(1);
	private Semaphore postiLiberi;
	
	public PiscinaSem(int numCorsie, int maxPersone) {
		super(numCorsie, maxPersone);
		postiLiberi = new Semaphore(corsie.length * maxPersone, true);
	}
	
	public int entra() throws InterruptedException {
		postiLiberi.acquire();
		mutex.acquire();
		int corsia = scegliCorsia(); //indice della corsia più libera
		corsie[corsia]++;
		mutex.release();
		return corsia;
	}
	
	public void esci(int numCorsia) throws InterruptedException {
		mutex.acquire();
		corsie[numCorsia]--;
		mutex.release();
		postiLiberi.release();
	}
	
	public static void main(String[] args) {
		System.exit(0); //
	}
}

