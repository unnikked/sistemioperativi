package appelli.piscina;

/**
	Ogni persona impiega da 3 a 5 minuti per
	cambiarsi, poi raggiunge la piscina ed entra nella corsia con il minor numero di persone: se tutte le corsie
	sono al completo attende in una coda FIFO che si liberi un posto in una corsia. Quando riesce ad entrare in
	una corsia la impegna per 45 minuti, poi libera la corsia, quindi si fa la doccia, impiegando per questo da 10
	a 15 minuti, dopodiché va via.
*/

import java.util.*;
import java.util.concurrent.*;

public class Persona extends Thread {
	private final int MIN_CAMBIO = 3,
					  MAX_CAMBIO = 5,
					  T_NUOTO = 45,
					  MIN_DOCCIA = 10,
					  MAX_DOCCIA = 15;
	private int corsia;
	private Piscina p;
	
	public Persona(Piscina p) {
		this.p = p;
	}
	
	public void run() {
		try {
			cambio();
			corsia = p.entra();
			TimeUnit.MINUTES.sleep(T_NUOTO); //nuoto
			p.esci(corsia);
			doccia();
		} catch(InterruptedException e) {
			//
		}
	}
	
	public void cambio() throws InterruptedException {
		TimeUnit.MINUTES.sleep(new Random().nextInt(MAX_CAMBIO - MIN_CAMBIO + 1) + MIN_CAMBIO);
	}
	
	public void doccia() throws InterruptedException {
		TimeUnit.MINUTES.sleep(new Random().nextInt(MAX_DOCCIA - MIN_DOCCIA + 1) + MIN_DOCCIA);
	}
	
}
