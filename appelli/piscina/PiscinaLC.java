package appelli.piscina;

/**
	Ogni persona impiega da 3 a 5 minuti per
	cambiarsi, poi raggiunge la piscina ed entra nella corsia con il minor numero di persone: se tutte le corsie
	sono al completo attende in una coda FIFO che si liberi un posto in una corsia. Quando riesce ad entrare in
	una corsia la impegna per 45 minuti, poi libera la corsia, quindi si fa la doccia, impiegando per questo da 10
	a 15 minuti, dopodiché va via.
*/

import java.util.*;
import java.util.concurrent.locks.*;

public class PiscinaLC extends Piscina {
	private Lock l = new ReentrantLock();
	private Condition possoEntrare = l.newCondition();
	private LinkedList<Thread> listaAttesa = new LinkedList<>();
	private int postiLiberi;
	
	public PiscinaLC(int numCorsie, int maxPersone) {
		super(numCorsie, maxPersone);
		postiLiberi = numCorsie * maxPersone;
	}
	
	public int entra() throws InterruptedException {
		l.lock();
		int corsia = 0;
		try {
			listaAttesa.addLast(Thread.currentThread());
			while(!ciSonoPostiLiberi()) {
				possoEntrare.await();
			}
			listaAttesa.removeFirst();
			postiLiberi--;
			corsia = scegliCorsia();
			corsie[corsia]++;
		} finally {
			l.unlock();
		}
		return corsia;
	}
	
	private boolean ciSonoPostiLiberi() {
		return postiLiberi > 0 && listaAttesa.getFirst() == Thread.currentThread();
	}
	
	public void esci(int numCorsia) throws InterruptedException {
		l.lock();
		try {
			corsie[numCorsia]--;
			possoEntrare.signalAll();
		} finally {
			l.unlock();
		}
	}
	
	public static void main(String[] args) {
		System.exit(0);//chiurito
	}
}
