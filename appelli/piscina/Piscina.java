package appelli.piscina;

/**
	Delle persone frequentano una piscina olimpionica per nuotare. La piscina ha N corsie, ciascuna delle quali
	può essere utilizzata al più da X persone contemporaneamente. Ogni persona impiega da 3 a 5 minuti per
	cambiarsi, poi raggiunge la piscina ed entra nella corsia con il minor numero di persone: se tutte le corsie
	sono al completo attende in una coda FIFO che si liberi un posto in una corsia. Quando riesce ad entrare in
	una corsia la impegna per 45 minuti, poi libera la corsia, quindi si fa la doccia, impiegando per questo da 10
	a 15 minuti, dopodiché va via.
	Si modelli il sistema descritto in Java, dove le persone sono dei thread che interagiscono tramite un oggetto
	piscina. La piscina esporta due metodi, entra ed esci, che permettono rispettivamente di entrare e di uscire
	dalla piscina. Si implementino due soluzioni che riproducano il funzionamento del problema sopra descritto
	utilizzando:
		3. la classe Semaphore del package java.util.concurrent
		4. gli strumenti di mutua esclusione e sincronizzazione del package java.util.concurrent.locks.
	Si scriva infine un main d'esempio che, facendo uso di una delle due soluzioni precedenti, inizializzi una
	piscina con N=10 ed X=4, inizializzi 80 persone e ne avvii l’esecuzione.
*/

public abstract class Piscina {
	protected int numCorsie;
	protected int maxPersone;
	protected int[] corsie;
	
	public Piscina(int numCorsie, int maxPersone) {
		this.numCorsie = numCorsie;
		this.maxPersone = maxPersone;
		corsie = new int[numCorsie];
	}
	
	public abstract int entra() throws InterruptedException;
	
	protected int scegliCorsia() { // il minimo corrisponde alla corsia più libera
		int indice = 0;
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < corsie.length; i++) {
			if(corsie[i] < min && corsie[i] + 1 <= maxPersone) {
				min = corsie[i];
				indice = i;
			}
		}
		return indice;
	}
	
	public abstract void esci(int numCorsia) throws InterruptedException;
}
