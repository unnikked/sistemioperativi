package appelli.pedaggio;

/**
	Ad ogni pagamento, la variabile incasso del casello deve essere incrementata di una quantità pari a x * T.
*/

import java.util.*;
import java.util.concurrent.locks.*;

public class CaselloLC extends Casello {
	private Lock l = new ReentrantLock();
	private Condition possoEntrare[];
	private LinkedListThread[] codaAttesa;
	private boolean[] occupato;
	
	public CaselloLC(int numPorte) {
		super(numPorte);
		possoEntrare = new Condition[numPorte];
		codaAttesa = new LinkedListThread[numPorte];
		occupato = new boolean[numPorte];
		for(int i = 0; i < numPorte; i++) {
			codaAttesa[i] = new LinkedListThread();
			possoEntrare[i] = l.newCondition();
		}
	}
	
	public void entraCasello(int nPorta) throws InterruptedException {
		l.lock();
		try {
			codaAttesa[nPorta].addLast(Thread.currentThread());
			while(occupato(nPorta)) {
				possoEntrare[nPorta].await();
			}
			occupato[numPorte] = true;
		} finally {
			l.unlock();
		}
	}
	
	private boolean occupato(int nPorta) {
		return occupato[nPorta];
	}
	
	public void esciCasello(int nPorta, int chilometri) throws InterruptedException {
		l.lock();
		try {
			while(!possoPagare(nPorta)) {
				possoEntrare[nPorta].await();
			} 
			codaAttesa[nPorta].removeFirst();
			occupato[nPorta] = false;
			incasso += tariffa * chilometri;
			possoEntrare[nPorta].signalAll();
		} finally {
			l.unlock();
		}
	}
	
	private boolean possoPagare(int nPorta) {
		return Thread.currentThread() == codaAttesa[nPorta].getFirst();
	}
	
	public static void main(String[] args) {
		//chiurito
	}
}

@SuppressWarnings("serial")
class LinkedListThread extends LinkedList<Thread> {
	//
}
