package appelli.pedaggio;

/**
	Ad ogni pagamento, la variabile incasso del casello deve essere incrementata di una quantità pari a x * T.
*/

import java.util.*;
import java.util.concurrent.*;

public class CaselloSem extends Casello {
	private Semaphore porte[]; 
	private Semaphore mutex;
	
	public CaselloSem(int numPorte) {
		super(numPorte);
		porte = new Semaphore[numPorte];
		for(int i = 0; i < numPorte; i++) {
			porte[i] = new Semaphore(1, true);
		}
		mutex = new Semaphore(1);
	}
	
	public void entraCasello(int nPorta) throws InterruptedException {
		porte[nPorta].acquire();
	}
	
	public void esciCasello(int nPorta, int chilometri) throws InterruptedException {
		mutex.acquire();
		incasso += tariffa * chilometri;
		mutex.release();
		porte[nPorta].release();
	}
	
	public static void main(String[] args) {
		//chiurito
	}
}
