package appelli.pedaggio;

/**
	Un casello autostradale è composto da N porte dove i veicoli pagano un pedaggio calcolato secondo una
	tariffa chilometrica T. Il casello include una variabile incasso che memorizza l’ammontare di denaro
	riscosso nella giornata attraverso tutte le porte.
	Ogni veicolo effettua una volta sola le seguenti operazioni:
		1. percorre un tratto di autostrada di x chilometri (con x compreso tra 50 e 100), impiegando 40 secondi
		   per ogni chilometro;
		2. giunto al casello, sceglie a caso la porta p a cui accodarsi;
		3. accede alla porta p, dopo aver atteso in ordine FIFO che i precedenti veicoli abbiano completato le
		   operazioni di pagamento su quella porta;
		4. impiega tra 3 e 6 secondi per effettuare il pagamento;
		5. rilascia la porta p ed abbandona l’autostrada.
	Ad ogni pagamento, la variabile incasso del casello deve essere incrementata di una quantità pari a x * T.
	Si modellino in Java i veicoli attraverso dei Thread e si implementino due soluzioni che riproducano il
	funzionamento del problema sopra descritto utilizzando:
		1. la classe Semaphore del package java.util.concurrent
		2. gli strumenti di mutua esclusione e sincronizzazione del package java.util.concurrent.locks.
	Si scriva infine un main d'esempio che faccia uso di una delle due soluzioni precedenti. A tal fine si
	inizializzi un certo numero V di veicoli, assegnando ad N ed a T dei valori a scelta dello studente.
*/

import java.util.LinkedList;

public abstract class Casello {
	protected int incasso;
	protected int tariffa = 7;
	protected int numPorte;
	
	public Casello(int numPorte) {
		this.numPorte = numPorte;
	}
	
	public int getNumPorte() {
		return numPorte;
	}
	
	public abstract void entraCasello(int porta) throws InterruptedException;
	
	public abstract void esciCasello(int porta, int chilometri) throws InterruptedException;
}
