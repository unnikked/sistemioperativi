package appelli.pedaggio;

/**
	Ogni veicolo effettua una volta sola le seguenti operazioni:
		1. percorre un tratto di autostrada di x chilometri (con x compreso tra 50 e 100), impiegando 40 secondi
		   per ogni chilometro;
		2. giunto al casello, sceglie a caso la porta p a cui accodarsi;
		3. accede alla porta p, dopo aver atteso in ordine FIFO che i precedenti veicoli abbiano completato le
		   operazioni di pagamento su quella porta;
		4. impiega tra 3 e 6 secondi per effettuare il pagamento;
		5. rilascia la porta p ed abbandona l’autostrada.
*/

import java.util.Random;
import java.util.concurrent.*;

public class Veicolo implements Runnable {
	private Casello c;
	private final int MIN_KM = 50;
	private final int MAX_KM = 100;
	private final int T_KM = 1;
	private final int MIN_ATTESA = 3;
	private final int MAX_ATTESA = 6;
	private int chilometri, porta;
	
	public Veicolo(Casello c) {
		this.c = c;
		this.porta = new Random().nextInt(c.getNumPorte());
	}
	
	public void run() {
		try {
			percorriAutostrada();
			c.entraCasello(porta);
			pagaPedaggio();
			c.esciCasello(porta, chilometri);
		}catch(InterruptedException e) {
			//
		}
	}
	
	private void percorriAutostrada() throws InterruptedException {
		chilometri = new Random().nextInt((MAX_KM - MIN_KM + 1) + MIN_KM);
		TimeUnit.SECONDS.sleep(T_KM * chilometri);
	}
	
	private void pagaPedaggio() throws InterruptedException {
		TimeUnit.SECONDS.sleep(new Random().nextInt((MAX_ATTESA - MIN_ATTESA + 1) + MIN_ATTESA));
	}
}
