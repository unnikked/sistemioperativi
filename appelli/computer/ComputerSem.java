package appelli.computer;

/**
	Le seguenti regole devono essere rispettate nell’allocare i core agli utenti:
		1) se un utente non trova liberi tutti i core che ha richiesto, viene messo in attesa senza occuparne
		alcuno (non sono ammesse allocazioni parziali);
		2) le richieste degli utenti devono essere soddisfatte in ordine FIFO.
*/

import java.util.*;
import java.util.concurrent.*;

public class ComputerSem extends Computer {
	private Semaphore[] processori = new Semaphore[2];
	
	public ComputerSem() {
		//super();
		processori[0] = new Semaphore(4, true);
		processori[1] = new Semaphore(4, true);
	}
	
	public void richiedi(int numCore, int nproc) throws InterruptedException {
		processori[nproc].acquire(numCore);
	}
	
	public void rilascia(int numCore, int nproc) throws InterruptedException {
		processori[nproc].release(numCore);
	}
	
	public static void main(String[] args) {
		//new ComputerSem().test(20);	
	}
}
