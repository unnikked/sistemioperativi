package appelli.computer;

/**
	Ciclicamente, ogni utente chiede al computer
	l’assegnazione di un certo numero di core di uno specifico processore. Per esempio, la richiesta (3,0) indica
	che l’utente ha bisogno di 3 core del processore 0. Ottenuti tutti i core richiesti, l’utente li utilizza per 1
	secondo; quindi, li rilascia ed aspetta 5 secondi prima di effettuare la richiesta successiva.
*/

import java.util.*;
import java.util.concurrent.*;

public class Utente extends Thread {
	private Computer c;
	private int numCore = new Random().nextInt(4) + 1;
	private int proc = new Random().nextInt(2);
	
	public Utente(Computer c) {
		this.c = c;
	}
	
	public void run() {
		while(true) {
			try {
				c.richiedi(numCore, proc);
				sleep(1000);
				c.rilascia(numCore, proc);
				sleep(5000);
			}catch(InterruptedException e) {
				//
			}
		}
	}
	
}
