package appelli.computer;

/**
	Le seguenti regole devono essere rispettate nell’allocare i core agli utenti:
		1) se un utente non trova liberi tutti i core che ha richiesto, viene messo in attesa senza occuparne
		alcuno (non sono ammesse allocazioni parziali);
		2) le richieste degli utenti devono essere soddisfatte in ordine FIFO.
*/

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

public class ComputerLC extends Computer {
	private Lock l = new ReentrantLock();
	private Condition possoRichiedere = l.newCondition();
	private LinkedList<Thread> listaAttesa = new LinkedList<>();
	
	public void richiedi(int numCore, int nproc) throws InterruptedException {
		l.lock();
		try {
			listaAttesa.addLast(Thread.currentThread());
			while(!ciSonoRisorse(numCore, nproc)) {
				possoRichiedere.await();
			}
			listaAttesa.removeFirst();
			proc[nproc] -= numCore;
		} finally {
			l.unlock();
		}
	}
	
	public void rilascia(int numCore, int nproc) throws InterruptedException {
		l.lock();
		try {
			proc[nproc] += numCore;
			possoRichiedere.notifyAll();;
		} finally {
			l.unlock();
		}
	}
	
	private boolean ciSonoRisorse(int numCore, int nproc) {
		return numCore <= proc[nproc];
	}
	
	public static void main(String[] args) {
		//chiurito
	}
}
