package appelli.computer;

/**
	Un computer contiene due processori, identificati dai numeri 0 e 1; ogni processore è a sua volta costituito
	da quattro core. Il computer è condiviso tra 20 utenti. Ciclicamente, ogni utente chiede al computer
	l’assegnazione di un certo numero di core di uno specifico processore. Per esempio, la richiesta (3,0) indica
	che l’utente ha bisogno di 3 core del processore 0. Ottenuti tutti i core richiesti, l’utente li utilizza per 1
	secondo; quindi, li rilascia ed aspetta 5 secondi prima di effettuare la richiesta successiva.
	Le seguenti regole devono essere rispettate nell’allocare i core agli utenti:
		1) se un utente non trova liberi tutti i core che ha richiesto, viene messo in attesa senza occuparne
		alcuno (non sono ammesse allocazioni parziali);
		2) le richieste degli utenti devono essere soddisfatte in ordine FIFO.
	Si modellino in Java gli utenti attraverso dei Thread e si implementino due soluzioni che riproducano il
	funzionamento del problema sopra descritto utilizzando:
		1. la classe Semaphore del package java.util.concurrent
		2. gli strumenti di mutua esclusione e sincronizzazione del package java.util.concurrent.locks.
	Si scriva infine un main d'esempio che faccia uso di una delle due soluzioni precedenti.
*/

public abstract class Computer {
	protected int[] proc = new int[2];
	
	public Computer() {
		proc[0] = 4;
		proc[1] = 4;
	}
	
	public abstract void richiedi(int numCore, int proc) throws InterruptedException;
	public abstract void rilascia(int numCore, int proc) throws InterruptedException;
	
	public void test(int numUtenti) {
		for(int i = 0; i < numUtenti; i++) {
			new Utente(this).start();
		}
	}
}
