package appelli.ristorante;

/**
	Sia il contenitore che lo
	scolapiatti hanno una dimensione finita, pari a 50 e 30 posti
	rispettivamente
*/

public abstract class Buffer {
	public enum Tipo {	CONTENITORE, SCOLAPIATTI };
	protected int numPiatti = 0;
	protected final int dimensione;
	protected Tipo tipo;
		
	public Buffer(int dimensione, Tipo t) {
		this.dimensione = dimensione;
		this.tipo = t;
	}
	
	public abstract void put(int n) throws InterruptedException;
	public abstract void get() throws InterruptedException;
	
}
