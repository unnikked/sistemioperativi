# Ristorante

In un ristorante lavorano dei camerieri, dei lavapiatti e degli
asciugapiatti. I camerieri raccolgono ciclicamente i piatti dai
tavoli del ristorante e li depositano in un contenitore di piatti
sporchi; i lavapiatti prelevano i piatti dal contenitore, li lavano e
li depositano in uno scolapiatti; gli asciugapiatti li prelevano
dallo scolapiatti e li asciugano. 

Sia il contenitore che lo
scolapiatti hanno una dimensione finita, pari a **50** e **30** posti
rispettivamente, per cui quando sono pieni chi cerca di
depositare si deve sospendere in attesa che si liberi un posto, e
quando sono vuoti chi cerca di prelevare deve sospendersi in
attesa che ci sia un piatto da prelevare. Il tempo di raccolta, di
lavaggio e di asciugatura è rispettivamente di _20_, _15_ e _10_
secondi. 

I camerieri raccolgono 4 piatti alla volta, mentre i
lavapiatti e gli asciugapiatti lavano ed asciugano un piatto alla
volta.
Si modelli il sistema descritto in Java, dove i camerieri, i
lavapiatti e gli asciugapiatti sono dei Thread che
interagiscono tramite due oggetti, contenitore e scolapiatti. 

Il contenitore e lo scolapiatti sono due istanze distinte di un'unica
classe che esporta due metodi, put e get, che permettono
rispettivamente di depositare e prelevare i piatti dall'oggetto.
Si implementino due soluzioni che riproducano il
funzionamento del problema sopra descritto utilizzando:

* La classe **Semaphore** del package _java.util.concurrent_
* Gli strumenti di **mutua esclusione** e **sincronizzazione** del
package _java.util.concurrent.locks_.

Si scriva infine un main d'esempio che, facendo uso di una delle
due soluzioni precedenti, inizializzi **10** camerieri, **6** lavapiatti e
**3** asciugapiatti, e ne avvii l’esecuzione.



