package appelli.ristorante;

/*
	I lavapiatti prelevano i piatti dal contenitore, li lavano e
	li depositano in uno scolapiatti;
	Tempo Lavaggio = 15;
*/
import java.util.concurrent.*;

public class LavaPiatti extends Thread {
	private Buffer contenitore;
	private Buffer scolapiatti;
	private static int TEMPO_LAVAGGIO = 15;
	
	public LavaPiatti(Buffer contenitore, Buffer scolapiatti) {
		this.contenitore = contenitore;
		this.scolapiatti = scolapiatti;
	}
	
	public void run() {
		while(true) {
			try {
				contenitore.get();
				TimeUnit.SECONDS.sleep(TEMPO_LAVAGGIO);
				scolapiatti.put(1);
			} catch(InterruptedException e) {
				
			}
		}
	}
	
	public String toString() {
		return "LavaPiatti#" + getId();
	}
}
