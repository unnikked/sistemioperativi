package appelli.ristorante;

/**
	Gli asciugapiatti li prelevano
	dallo scolapiatti e li asciugano.
	Tempo asciugatura = 10;
*/

import java.util.concurrent.*;

public class AsciugaPiatti extends Thread {
	private Buffer scolapiatti;
	private static int TEMPO_ASCIUGATURA = 10;
	
	public AsciugaPiatti(Buffer scolapiatti) {
		this.scolapiatti = scolapiatti;
	}
	
	public void run() {
		while(true) {
			try {
				scolapiatti.get();
				TimeUnit.SECONDS.sleep(TEMPO_ASCIUGATURA);
			} catch(InterruptedException e) {
				
			}
		}
	}
	
	public String toString() {
		return "AsciugaPiatti#" + getId();
	}
}
