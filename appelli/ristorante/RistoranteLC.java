package appelli.ristorante;

/**
	Sia il contenitore che lo scolapiatti hanno
	una dimensione finita, pari a 50 e 30 posti
	rispettivamente, per cui quando sono pieni 
	chi cerca di depositare si deve sospendere 
	in attesa che si liberi un posto, e quando 
	sono vuoti chi cerca di prelevare deve 
	sospendersi in attesa che ci sia un piatto 
	da prelevare.
*/

import java.util.concurrent.locks.*;

public class RistoranteLC extends Buffer {
	private Lock mutex = new ReentrantLock();
	private Condition bufferNonVuoto = mutex.newCondition();
	private Condition bufferNonPieno = mutex.newCondition();
	
	public RistoranteLC(int dimensione, Tipo tipo) {
		super(dimensione, tipo);
	}
	
	public void put(int n) throws InterruptedException {
		mutex.lock();
		try {
			while(numPiatti + n > dimensione) {
				bufferNonVuoto.await();
			}
			numPiatti += n;
			System.out.println(Thread.currentThread().toString() + " inserisce " + n + " piatti in " + this.tipo + " TOT: " + numPiatti);
			bufferNonPieno.signal();
		} finally {
			mutex.unlock();
		}
	}
	
	public void get() throws InterruptedException {
		mutex.lock();
		try {
			while(numPiatti == 0) {
				bufferNonPieno.await();
			}
			numPiatti--;
			System.out.println(Thread.currentThread().toString() + " preleva 1 piatto da " + this.tipo + " TOT: " + numPiatti);
			bufferNonVuoto.signal();
		} finally {
			mutex.unlock();
		}
	}
	
	public static void main(String[] args) {
		Buffer contenitore = new RistoranteLC(50, Tipo.CONTENITORE);
		Buffer scolapiatti = new RistoranteLC(30, Tipo.SCOLAPIATTI);
		int numCamerieri = 10;
		int numLavaPiatti = 6;
		int numAsciugaPiatti = 3;
		
		for(int i = 0; i < numCamerieri; i++) {
			new Cameriere(contenitore).start();
		}
		for(int i = 0; i < numLavaPiatti; i++) {
			new LavaPiatti(contenitore, scolapiatti).start();
		}
		for(int i = 0; i < numAsciugaPiatti; i++) {
			new AsciugaPiatti(scolapiatti).start();
		}
	}
}
