package appelli.ristorante;

/**
	Sia il contenitore che lo scolapiatti hanno
	una dimensione finita, pari a 50 e 30 posti
	rispettivamente, per cui quando sono pieni 
	chi cerca di depositare si deve sospendere 
	in attesa che si liberi un posto, e quando 
	sono vuoti chi cerca di prelevare deve 
	sospendersi in attesa che ci sia un piatto 
	da prelevare.
*/

import java.util.concurrent.*;

public class RistoranteSem extends Buffer {
	private Semaphore mutex = new Semaphore(1);
	private Semaphore possoInserire;
	private Semaphore possoPrelevare;
	
	public RistoranteSem(int dimensione, Tipo tipo) {
		super(dimensione, tipo);
		possoInserire = new Semaphore(dimensione);
		possoPrelevare = new Semaphore(0);
	}
	
	public void put(int n) throws InterruptedException {
		possoInserire.acquire(n);
		mutex.acquire();
		numPiatti += n;
		System.out.println(Thread.currentThread().toString() + " inserisce " + n + " piatti in " + this.tipo + " TOT: " + numPiatti);
		possoPrelevare.release(n);
		mutex.release();
	}
	
	public void get() throws InterruptedException {
		possoPrelevare.acquire();
		mutex.acquire();
		numPiatti--;
		System.out.println(Thread.currentThread().toString() + " preleva 1 piatto da " + this.tipo + " TOT: " + numPiatti);
		possoInserire.release();
		mutex.release();
	}
	
	public static void main(String[] args) {
		RistoranteSem contenitore = new RistoranteSem(50, Tipo.CONTENITORE);
		Buffer scolapiatti = new RistoranteSem(30, Tipo.SCOLAPIATTI);
		int numCamerieri = 10;
		int numLavaPiatti = 6;
		int numAsciugaPiatti = 3;
		
		for(int i = 0; i < numCamerieri; i++) {
			new Cameriere(contenitore).start();
		}
		for(int i = 0; i < numLavaPiatti; i++) {
			new LavaPiatti(contenitore, scolapiatti).start();
		}
		for(int i = 0; i < numAsciugaPiatti; i++) {
			new AsciugaPiatti(scolapiatti).start();
		}
			
	}
}
