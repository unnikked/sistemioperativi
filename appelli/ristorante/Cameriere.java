package appelli.ristorante;
/**
	I camerieri raccolgono ciclicamente i piatti
	dai tavoli del ristorante e li depositano nel
	contenitore dei piatti.
	TEMPO RACCOLTA = 20 secondi
	Raccolgono 4 piatti per volta
*/

import java.util.concurrent.*;

public class Cameriere extends Thread {
	private	Buffer contenitore;
	private final int TEMPO_RACCOLTA = 20;
	private final int NUM_PIATTI = 4;
	
	public Cameriere(Buffer contenitore) {
		this.contenitore = contenitore;
	}
	
	public void run() {
		while(true) {
			try {
				TimeUnit.SECONDS.sleep(TEMPO_RACCOLTA);
				contenitore.put(NUM_PIATTI);
			} catch(InterruptedException e) {
				
			}
		}
	}
	
	public String toString() {
		return "Cameriere#" + getId();
	}
}
