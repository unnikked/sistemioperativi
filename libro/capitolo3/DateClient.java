import java.net.*;
import java.io.*;

public class DateClient {
	public static void main(String[] args) {
		try {
			// si collega alla porta su cui ascolta il server
			Socket sock = new Socket("127.0.0.1", 6013);
			
			InputStream in = sock.getInputStream();
			BufferedReader bin = new
			 BufferedReader(new InputStreamReader(in));
			 
			// legge la data dalla socket
			String line;
			while((line = bin.readLine()) != null)
				System.out.println(line);
			
			// chiude la connessione tramite socket
			sock.close();	
		} catch(IOException ioe) {
			System.err.println(ioe);
		}	
	}
}
