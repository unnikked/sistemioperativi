#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>

/**
	Programma C che illustra la API POSIX
	per la condivisione della memoria
*/

int main()
{
	/* identificatore del segmento condiviso */
	int segment_id;
	/* puntatore al segmento condiviso */
	char* shared_memory;
	/* dimensione in byte del segmento condiviso */
	const int size = 4096;
	
	/* alloca il segmento condiviso */
	segment_id = shmget(IPC_PRIVATE, size, S_IRUSR | S_IWUSR);
	
	/* annette il segmento condiviso */
	shared_memory = (char*) shmat(segment_id, NULL, 0);
	
	/* scrive un messaggio nel segmento condiviso */
	printf("*%s\n", shared_memory);
	
	/* elimina il segmento condiviso dal proprio spazio indirizzi */
	shmdt(shared_memory);
	
	/* elimina il segmento condiviso dal sistema */
	shmctl(segment_id, IPC_RMID, NULL);
	
	return 0;
}
