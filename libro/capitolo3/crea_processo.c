#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
	pid_t pid;

	/* genera un nuovo processo */
	pid = fork();
	
	if(pid < 0) { //errore
		fprintf(stderr, "Generazione del nuovo processo fallita");
		return 1;
	} else if(pid == 0) { //processo figlio
		execlp("/bin/ls", "ls", NULL);
	} else { // processo padre
		/* il genitore attende il completamento del figlio */
		wait(NULL);
		printf("Il processo figlio ha terminato \n");
	}
	return 0;
}
