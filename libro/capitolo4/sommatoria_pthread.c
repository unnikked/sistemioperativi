#include <pthread.h>
#include <stdio.h>

long long sum; /* questo dato è condiviso dai thread */
void *runner(void *param);

int main(int argc, char *argv[])
{
	pthread_t tid; /* identificatore del thread */
	pthread_attr_t attr; /* l'insieme degli attributi del thread*/
	
	if(argc != 2) {
		fprintf(stderr, "uso: sommatoria_phtread.o <valore intero>\n");
		return -1;
	}
	
	if(atoi(argv[1]) < 0) {
		fprintf(stderr, "%d deve essere >= 0\n", atoi(argv[1]));
		return -1;
	}
	
	/* reperisce gli attributi predefiniti */
	pthread_attr_init(&attr);
	/* crea il thread */
	pthread_create(&tid, &attr, runner, argv[1]);
	/* attende la terminazione del thread */
	pthread_join(tid, NULL);
	
	printf("sum = %lld\n", sum);
}

/* il thread assume il controllo da questa funzione */
void *runner(void *param) 
{
	int i, upper = atoi(param);
	sum = 0;
	
	for(i = 1; i <= upper; i++)
		sum += i;
		
	pthread_exit(0);
}
