package libro.algoritmoBanchiere;

/**
	Implementazione molto schematica e naive 
	dell'algoritmo del Banchiere
*/

import java.util.Random;

/**
	Vettore avaiable -> indica le risorse rimaste disponibili nel sistema
	Vettore work -> indica le risorse rimaste disponibili nel sistema nel momento della verifica di statoSicuro
	Vettore finish -> se per ogni i finish[i] == true il sistema è in statoSicuro
	
	Matrice max -> indica il numero massimo ri risorse che può richiedere un processo i
	Matrice allocation -> indica le risorse allocate al processo i
	Matrice need -> indica le risorse che potrebbero servire al processo i

*/

public class Banchiere {
	private int nProcessi, mRisorse;
	private int[] avaiable, work;
	private boolean[] finish;
	private int[][] max, allocation, need;
	
	public Banchiere(int n, int m) {
		nProcessi = n;
		mRisorse = m;
		avaiable = new int[m];
		max = new int[n][m];
		allocation = new int[n][m];
		need = new int[n][m];
		for(int i = 0; i < avaiable.length; i++)
			avaiable[i] = new Random.nextInt(10);
		for(int i = 0; i < avaiable.length; i++)
			need[i] = sottrai(max[i], allocation[i]);
		for(int i = 0; i < n; i++)
			new Processo(this, i).start();
	}
	
	public int getRisorse() {
		return mRisorse;
	}
	
	public boolean assegnaRisorse(int[] request, int pId) {
		if(!minore(request, need[pId])) return false; //il processo richiede più risorse che il sistema ha
		if(!minore(request, avaiable[pId])) return false; //il processo deve aspettare non ci sono risorse disponibili
		simulaAssegnazione(request, pId);
		/* se non si verificano le condizioni di stato sicuro
		   il processo deve attendere (qui non lo faccio) */
		if(!statoSicuro(pId)) {
			rollBack(request, pId);
			return false;
		} 
		return true;
	}
	
	private void simulaAssegnazione(int[] request, int pId) {
		avaiable = sottrai(avaiable, request);
		allocation[pId] = somma(allocation[pId], request);
		need[pId] = sottrai(need[pId], request);
	}
	
	private boolean statoSicuro(int pId) {
		work = avaiable;
		finish = new boolean[nProcessi];
		for(int i = 0; i < finish.length; i++)
			finish[i] = false;
		/* si cerca un i tale che sia finish[i] == true && need[i] <= work */
		for(int i = 0; i < finish.length; i++) {
			if(!finish[i] && minore(need[i], work)) /*need[i] <= work */{
				work = somma(work, allocation[i]);
				finish[i] = true;
			} else break; // i non esiste e si vede se c'è uno statoSicuro
		}
		/* determina lo statoSicuro */
		for(int i = 0; i < finish.length; i++) {
			if(!finish[i]) return false;
		}
		return true;
	}
	
	private void rollBack(int[] request, int pId) {
		avaiable = somma(avaiable, request);
		allocation[pId] = sottrai(allocation[pId], request);
		need[pId] = somma(need[pId], request, false);
	}
	
	public void rilasciaRisorse(int[] request, int pId) {
		rollBack(request, pId);
	}	
}

class Processo extends Thread {
	private Banchiere b;
	private int pId;
	
	public Processo(Banchiere b, int pId) {
		this.b = b;
		this.pId = pId;
	} 
	
	public void run() {
		try {
			int[] request = new int[b.getRisorse()];
			for(int i = 0; i < request.length; i++) {
				request[i] = new Random().nextInt(5);
			}
			b.assegnaRisorse(request, pId);
			//operazioni
			b.rilasciaRisorse(request, pId);
		}catch(InterruptedException e) {
			///
		} 
	}//
}
